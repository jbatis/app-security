from flask_restplus import Api
from .views import api_view as authApi

api = Api(
    title='Auth API',
    version='1.0',
    description='Expose all the APIs that allow auth the user',
)

api.add_namespace(authApi, path="/api/auth")