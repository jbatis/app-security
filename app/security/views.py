from flask_restplus import Namespace, Resource, fields
from flask import request, jsonify
from app.security.models import User
from config import db

api_view = Namespace('loginServiceApi', description='Allow authentication from user, and get token if its successfuls')

@api_view.route('/getAuthUser')
@api_view.response(404, 'Service not avalible')
class LoginUserView(Resource):

    @api_view.param('username', 'Name of user that try to do login')
    @api_view.param('password', 'Password of user that try to do login')
    @api_view.param('tokenacces', 'Token that allow valid the service')
    def post(self):
        username = request.args.get('username', None) 
        password = request.args.get('password', None)
        tokenacces = request.args.get('tokenacces', None)
        print("username:{} password: {}, tokenacces: {}".format(username, password, tokenacces))
        if (username == password):
            return jsonify(
                {
                'idUser': username,
                'token': 'bar-token-example',
                'estatus': 'OK'
                }
            )
        else:
            return jsonify(
                {
                'idUser': username,
                'estatus': 'BAD'
                }
            )

@api_view.route('/doAdminUser')
@api_view.response(404, 'Service not avalible')
class CrudUserView(Resource):
    
    @api_view.param('user', 'Name user')
    @api_view.param('email', 'Email user')
    @api_view.param('password', 'Password user')
    @api_view.param('admin', 'Check if the user is admin or not, default is not admin')
    def post(self):
        user = request.args.get('user', None) 
        email = request.args.get('email', None) 
        password = request.args.get('password', None)
        isAdmin = request.args.get('admin', False)

        user = User(user=user, email=email, password=password, admin=isAdmin)
        db.session.add(user)
        db.session.commit()

