from flask import Flask
from config import config

def create_app(config_name):
    ''' Create factory method to init the app; views and apis '''
    app = Flask(__name__)
    ambiente = 'dev'
    app.logger.info('Config app in :: {}'.format(ambiente))
    app.config.from_object(config[ambiente])
    config[ambiente].init_app(app)
    return app
