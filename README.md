# Flask JWT Auth

[![Build Status](https://travis-ci.org/realpython/flask-jwt-auth.svg?branch=master)](https://travis-ci.org/realpython/flask-jwt-auth)

## Quick Start


### To run the app

# This allow run the app in DEBUG mode
```bash
$ export FLASK_DEBUG=1
$ export FLASK_ENV=development
```

#To set app in flask
```bash
$ export FLASK_APP=app_auth.py
```

#To generate the database:
```bash
$ flask db init
$ flask db migrate -m "To generate the database"
```

#To update the model
```bash
$ flask db upgrade
```

#to run the app
```bash
$ flask run
```

### Basics

1. Activate a virtualenv

```bash
$ python3 -m venv venv
$ source venv/bin/activate
```

1. Install the requirements

```bash
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

On Windows 
Install the virtualenv package

The virtualenv package is required to create virtual environments. You can install it with pip:
pip install virtualenv

virtualenv mypython

mypthon\Scripts\activate

### Create DB

```sql
-- DROP DATABASE flask_jwt_auth;

CREATE DATABASE flask_jwt_auth
    WITH 
    OWNER = uecp
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Mexico.1252'
    LC_CTYPE = 'Spanish_Mexico.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
```

Create the databases in `psql`:

```sh
$ psql
# create database flask_jwt_auth
# create database flask_jwt_auth_testing
# \q
```

Create the tables and run the migrations:

```sh
$ python app_auth.py db init
$ python app_auth.py db migrate
$ python app_auth.py db upgrade
$ python app_auth.py db --help
```
