import os
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from app.security import api as authApiView

basedir = os.path.abspath(os.path.dirname(__file__))

db = SQLAlchemy()
migrate = Migrate()
bcrypt = Bcrypt()

database_config = {
    'user': 'symemys',
    'pw': 'Symemys234',
    'db': 'symemys',
    'host': 'localhost',
    'port': '5431',
}

class Config:
    SECRET_KEY = '\x8d\xb4|\xff-\x04I\xf4^\xa6f+\x00\xb7\x8d,\xee\x07\xbd\x8e\xb7h\x890'
    DEBUG = True

    @staticmethod
    def init_app(app):
        app.logger.info('Base Security APP init...')
        '''Init the db '''
        db.init_app(app)
        '''Init the migrate service '''
        migrate.init_app(app, db)
        '''Init the bcrypt component '''
        bcrypt.init_app(app)
        '''Setup the views '''
        
        authApiView.init_app(app)

        return app

class DesaConfig(Config):
    """Development configuration."""
    BCRYPT_LOG_ROUNDS = 4
    #SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % database_config
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'flask_jwt_auth_tests.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False 

class TestConfig(Config):
    """Testing configuration."""
    TESTING = True
    BCRYPT_LOG_ROUNDS = 4
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'sqlite:///' + os.path.join(basedir, 'flask_jwt_auth_tests.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False

class QaConfig(Config):
    """QA configuration."""
    DEBUG = False
    SECRET_KEY = os.environ.get('SECRET_KEY') or Config.SECRET_KEY
    SQLALCHEMY_DATABASE_URI =  os.environ.get('DATABASE_URI') or 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % database_config


config = {
    'dev' : DesaConfig,
    'test' : TestConfig,
    'qa' : QaConfig
}