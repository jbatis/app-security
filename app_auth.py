import os
from app import create_app
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

#Create app, by default is mode development
#app_security = create_app(os.getenv('FLASK_ENV') or 'dev')
app_security = create_app('dev')

@app_security.cli.command()
def test():
    """ Run my tests of symemys """
    import unittest
    tests = unittest.TestLoader().discover('app/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

if __name__ == '__main__':
    from config import db
    from app.security import models
    # To migrate to daabase
    migrate = Migrate(app_security, db)
    # Mannaget run app
    manager = Manager(app_security)
    manager.add_command('db', MigrateCommand)
    manager.run()